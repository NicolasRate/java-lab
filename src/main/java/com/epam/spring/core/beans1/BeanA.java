package com.epam.spring.core.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanA {

    @Override
    public String toString() {
        return "BeanA";
    }
}
