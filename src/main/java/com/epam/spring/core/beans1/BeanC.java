package com.epam.spring.core.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {

    @Override
    public String toString() {
        return "BeanC";
    }
}
