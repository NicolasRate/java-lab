package com.epam.spring.core;

import com.epam.spring.core.beans1.BeanC;
import com.epam.spring.core.beans7.BeanCollection;
import com.epam.spring.core.beans7.MegaBean;
import com.epam.spring.core.config.AppConfig1;
import com.epam.spring.core.config.AppConfig2;
import com.epam.spring.core.config.AppConfig3;
import com.epam.spring.core.otherBeans.OtherMainBean1;
import com.epam.spring.core.otherBeans.OtherMainBean2;
import com.epam.spring.core.otherBeans.OtherMainBean3;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
//        Task3
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig1.class);
//        for (String beanDefinitionName : ctx.getBeanDefinitionNames()) {
//            System.out.println(beanDefinitionName);
//        }
//        Task4
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig2.class);
//        for (String beanDefinitionName : ctx.getBeanDefinitionNames()) {
//            System.out.println(beanDefinitionName);
//        }
//        Task6
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig3.class);
//        System.out.println(ctx.getBean(OtherMainBean1.class));
//        System.out.println(ctx.getBean(OtherMainBean2.class));
//        System.out.println(ctx.getBean(OtherMainBean3.class));
//        Task7,8
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig1.class);
//        ctx.getBean(BeanCollection.class).printBeans();
//        System.out.println(ctx.getBean(MegaBean.class).getBean1Bean2());
//        Task9
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig3.class);
//        for (String beanDefinitionName : ctx.getBeanDefinitionNames()) {
//            System.out.println(beanDefinitionName);
//        }
    }
}
