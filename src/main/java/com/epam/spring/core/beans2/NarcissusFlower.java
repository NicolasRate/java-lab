package com.epam.spring.core.beans2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {

    @Override
    public String toString() {
        return "NarcissusFlower";
    }
}
