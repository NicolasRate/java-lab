package com.epam.spring.core.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {

    @Override
    public String toString() {
        return "CatAnimal";
    }
}
