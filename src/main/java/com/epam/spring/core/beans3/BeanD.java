package com.epam.spring.core.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {

    @Override
    public String toString() {
        return "BeanD";
    }
}
