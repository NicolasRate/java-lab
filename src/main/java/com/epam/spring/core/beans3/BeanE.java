package com.epam.spring.core.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {

    @Override
    public String toString() {
        return "BeanE";
    }
}
