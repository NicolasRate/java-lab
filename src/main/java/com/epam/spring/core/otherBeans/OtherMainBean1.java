package com.epam.spring.core.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.print.attribute.standard.MediaSize;

@Component
public class OtherMainBean1 {

    private OtherBeanA beanA;

    @Autowired
    OtherMainBean1(OtherBeanA beanA) {
        this.beanA = beanA;
    }

    @Override
    public String toString() {
        return "OtherMainBean1: " + this.beanA;
    }

}
