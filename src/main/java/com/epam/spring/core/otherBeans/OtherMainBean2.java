package com.epam.spring.core.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OtherMainBean2 {

    private OtherBeanB beanB;

    @Autowired
    @Qualifier("OtherBeanB")
    public void setBeanB(OtherBeanB beanB) {
        this.beanB = beanB;
    }

    @Override
    public String toString() {
        return "OtherMainBean2: " + this.beanB;
    }
}
