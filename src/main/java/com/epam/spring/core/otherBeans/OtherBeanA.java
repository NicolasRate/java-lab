package com.epam.spring.core.otherBeans;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanA {

    @Override
    public String toString() {
        return "OtherBeanA";
    }
}
