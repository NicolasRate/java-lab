package com.epam.spring.core.otherBeans;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanC {

    @Override
    public String toString() {
        return "OtherBeanC";
    }
}
