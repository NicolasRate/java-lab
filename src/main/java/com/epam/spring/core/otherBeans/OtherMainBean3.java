package com.epam.spring.core.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OtherMainBean3 {

    @Autowired
    private OtherBeanC beanC;

    @Override
    public String toString() {
        return "OtherMainBean3: " + this.beanC;
    }
}
