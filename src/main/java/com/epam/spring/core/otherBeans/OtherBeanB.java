package com.epam.spring.core.otherBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("OtherBeanB")
public class OtherBeanB {

    @Override
    public String toString() {
        return "OtherBeanB";
    }
}
