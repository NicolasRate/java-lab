package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeanCollection {

    @Autowired
    private List<IBean> beans;

    public void printBeans() {
        for (IBean bean : beans) {
            System.out.println(bean.getBeanName());
        }
    }
}
