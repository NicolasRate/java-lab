package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
@Primary
public class Bean1 implements IBean {

    @Override
    public String getBeanName() {
        return "Bean1";
    }
}
