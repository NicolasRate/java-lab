package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Qualifier("Bean3")
public class Bean3 implements IBean {

    @Override
    public String getBeanName() {
        return "Bean3";
    }
}
