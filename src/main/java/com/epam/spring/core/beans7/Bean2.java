package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("Bean2")
public class Bean2 implements IBean {

    @Override
    public String getBeanName() {
        return "Bean2";
    }
}
