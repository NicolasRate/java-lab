package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MegaBean {

    private IBean bean1;
    private IBean bean2;
    private IBean bean3;
    private IBean bean4;

    @Autowired
    MegaBean(IBean bean1, @Qualifier("Bean2") IBean bean2,
             @Qualifier("Bean3") IBean bean3, @Qualifier("Bean4") IBean bean4) {
        this.bean1 = bean1;
        this.bean2 = bean2;
        this.bean3 = bean3;
        this.bean4 = bean4;
    }

    public String getBean1Bean2() {
        return bean1.getBeanName() + " : " + bean2.getBeanName();
    }

}
