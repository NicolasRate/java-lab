package com.epam.spring.core.beans7;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("Bean4")
public class Bean4 implements IBean {

    @Override
    public String getBeanName() {
        return "Bean4";
    }
}
