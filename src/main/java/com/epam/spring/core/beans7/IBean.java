package com.epam.spring.core.beans7;

public interface IBean {
    String getBeanName();
}
