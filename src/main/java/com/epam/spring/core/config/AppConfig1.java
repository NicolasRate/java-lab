package com.epam.spring.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.epam.spring.core.beans1")
@ComponentScan(basePackages = "com.epam.spring.core.beans7")
public class AppConfig1 {
}
