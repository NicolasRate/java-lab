package com.epam.spring.core.config;

import com.epam.spring.core.beans3.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.spring.core.beans2"),
        @ComponentScan(basePackages = "com.epam.spring.core.beans3",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = {BeanD.class, BeanF.class}))
})
public class AppConfig2 {
}
